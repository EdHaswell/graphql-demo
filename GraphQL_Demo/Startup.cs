using Autofac;
using GraphQL.Server;
using GraphQL_Demo.Data;
using GraphQL_Demo.GraphQl;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using GraphQL.SystemTextJson;


namespace GraphQL_Demo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
            .AddEntityFrameworkInMemoryDatabase()
            .AddDbContext<ContentContext>(context => { context.UseInMemoryDatabase("ContentDb"); });

            services.AddScoped<IContentRepository, ContentRepository>();
            services.AddScoped<IAuthorRepository, AuthorRepository>();
            services.AddScoped<AppSchema>();
            services.AddGraphQL()
                .AddSystemTextJson()
                .AddGraphTypes(typeof(AppSchema), ServiceLifetime.Scoped);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseGraphQL<AppSchema>();

            app.UseGraphQLPlayground();

            app.UseHttpsRedirection();

        }
    }
}
