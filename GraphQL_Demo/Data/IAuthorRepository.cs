﻿using App.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL_Demo.Data
{
    public interface IAuthorRepository
    {
        Task<List<Author>> GetAuthorsByName(string name);

        Task<List<Author>> GetAllAuthors();
    }

}
