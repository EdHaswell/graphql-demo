﻿using App.Entity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL_Demo.Data
{
    public class ContentRepository : IContentRepository
    {
        private readonly ContentContext context;

        public ContentRepository(ContentContext context)
        {
            this.context = context;
            this.context.Database.EnsureCreated();
        }

        public async Task<Content> GetContentByIdAsync(int id) 
        {
            return await context.Content.Where(c => c.ID == id).AsNoTracking().FirstOrDefaultAsync();
        }

        public async Task<List<Content>> GetAllContentAsync()
        {
            return await context.Content.AsNoTracking().ToListAsync();
        }
    }
}
