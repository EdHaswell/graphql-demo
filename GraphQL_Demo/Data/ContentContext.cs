﻿using App.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL_Demo.Data
{
    public class ContentContext : DbContext
    {
        public ContentContext(DbContextOptions<ContentContext> options) : base(options)
        {
        }

        public DbSet<Content> Content { get; set; }

        public DbSet<Author> Authors { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Content>().HasData(
                new Content 
                {
                    ID = 1,
                    ContentName = "GraphQl News",
                },
                new Content 
                {   
                    ID = 2,
                    ContentName = "Updates",
                }
            );

            modelBuilder.Entity<Author>().HasData(
                new Author
                {
                    ID = 1,
                    Firstname = "Ed",
                    LastName = "Haswell",
                    FullName =  "Ed Haswell"
        },
                new Author
                {
                    ID = 2,
                    Firstname = "Test",
                    LastName = "Two",
                    FullName = "Test Two"
                },
                new Author
                {
                    ID = 3,
                    Firstname = "John",
                    LastName = "Cena",
                    FullName = "John Cena"
                }
            );
        }
    }
}
