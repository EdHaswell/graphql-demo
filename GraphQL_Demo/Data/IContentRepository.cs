﻿using App.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL_Demo.Data
{
    public interface IContentRepository
    {
        Task<Content> GetContentByIdAsync(int id);

        Task<List<Content>> GetAllContentAsync();
    }
}
