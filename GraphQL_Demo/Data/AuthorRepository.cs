﻿using App.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL_Demo.Data
{
    public class AuthorRepository : IAuthorRepository
    {
        private readonly ContentContext context;

        public AuthorRepository(ContentContext context)
        {
            this.context = context;
            this.context.Database.EnsureCreated();
        }

        public async Task<List<Author>> GetAuthorsByName(string name) 
        {
            var authors = await context.Authors.Where(c => c.FullName.ToLower().Contains(name.ToLower()) ).AsNoTracking().ToListAsync();
            return authors;
        }

        public async Task<List<Author>> GetAllAuthors()
        {
            return await context.Authors.AsNoTracking().ToListAsync();
        }
    }
}
