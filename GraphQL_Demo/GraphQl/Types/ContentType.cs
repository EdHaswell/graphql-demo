﻿using App.Entity;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL_Demo.GraphQl.Types
{
    public sealed class ContentType : ObjectGraphType<Content>
    {
        public ContentType()
        {
            Name = nameof(Content);
            Description = "A piece of Content";

            Field(c => c.ID).Description("This Id of the content");
            Field(c => c.ContentName).Description("The Name of the content");
        }
    }
}
