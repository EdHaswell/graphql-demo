﻿using App.Entity;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL_Demo.GraphQl.Types
{
    public sealed class AuthorType : ObjectGraphType<Author>
    {
        public AuthorType()
        {
            Name = nameof(Author);
            Description = "The Authors of the content";

            Field(a => a.ID).Description("The id of the author");
            Field(a => a.Firstname).Description("First Name of the author");
            Field(a => a.LastName).Description("Last Name of the author");
            Field(a => a.FullName).Description("Full Name of the author");
        }
    }
}
