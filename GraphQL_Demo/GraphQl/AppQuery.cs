﻿using App.Entity;
using GraphQL;
using GraphQL.Types;
using GraphQL_Demo.Data;
using GraphQL_Demo.GraphQl.Types;
using System.Collections.Generic;

namespace GraphQL_Demo.GraphQl
{
    public class AppQuery : ObjectGraphType<object>
    {
        public AppQuery(IContentRepository contentRepository, IAuthorRepository authorRepository)
        {
            Field<ListGraphType<ContentType>>(
            "all_content",
            resolve: context => contentRepository.GetAllContentAsync());

            Field<ContentType>(
            "content",
            arguments: new QueryArguments(
                new QueryArgument<IdGraphType> { Name = "id" }),
            resolve: context => contentRepository.GetContentByIdAsync(context.GetArgument<int>("id")));

            Field<ListGraphType<AuthorType>>(
            "all_authors",
            resolve: context => authorRepository.GetAllAuthors());

            Field< ListGraphType<AuthorType>>(
            "author",
            arguments: new QueryArguments(
                new QueryArgument<StringGraphType> { Name = "name" }),
            resolve: context => authorRepository.GetAuthorsByName(context.GetArgument<string>("name")));
        }
    }
}
