﻿using GraphQL.Types;
using GraphQL_Demo.GraphQl.Types;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL_Demo.GraphQl
{
    public class AppSchema : Schema
    {
        public AppSchema(IServiceProvider sp) : base(sp)
        {
            Query = sp.GetRequiredService<AppQuery>(); ;
        }
    }

}
